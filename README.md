# README #

Ejercicio presentado al instituto Humboldt donde resuelve los siguiente:

Almacena los registros del archivo taxon.cvs en la tabla "species", 
Obtiene el campo nombre científico para todos los registros presentes.

obtener las ocurrencias disponibles mediante API del sitio web de GBIF (http://www.gbif.org/developer/occurrence#search), usando el nombre científico de especies almacenadas.

### Arquitectura ###

* Php > 5.6
* MySql > 5
* CakePhp 2.x
* requiere ext-mcrypt

### Puesta en funcionamiento  ###

para instalar y ejecutar la aplicación es recomendable un entorno LAMP (Linux-Apache-Mysql-Php), o MAMP (MAMP pro)
Hacer pull a este repositorio. 
Crear una base de datos llamada gdif
En la raiz del proyecto se encuentra el script para crear la base de datos "gdif_2016-03-02.sql", importar el script asociando a la base de datos creada anteriormente.
Crear un Host por ejemplo "gdif.dev" y con Apache apuntar a la carpeta webroot del proyecto, por ejemplo "/Users/alberto/www/**gbif/app/webroot**"
En el archivo app/database.php puede ajustar las variables de conexión a la base de datos:
 
* Host o unix_socket
* El usuario y contraseña de Mysql respectivamente: login y password

Una vez todo ajustado deberá poder acceder a la aplicación ingresando al Host creado previamente.

### Uso ###

Cargar especies: para cargar las especies del archivo taxon.cvs debera ingresar en "Cargar Especies", seguido el boton "Cargar"
lo que hace:
* Verifica que el archivo exista.
* Valida archivos duplicados en el archivo.
* En caso de la tabla species estar vacía, almacena los registros del archivo hacia la tabla.
* Almacena registros nuevos, no se crean duplicados

Para Cargar Occurrences, Ir a "Listar Occurrences" luego "Cargar Occurrences" -> "Encontrar Occurrences".
lo que hace:
* Busca todas las especies y via ajax se las pasa a un servicio que,
* Busca todas las Occurrences por especie a la vez
* Almacena y muestra en pantalla el resultado total por especie, a medida que las va encontrando.



### No desarrollado ###

* Procesar especies con mas de 200.000 ocurrencias

### Who do I talk to? ###

* Jose Alberto Gonzalez Rouille alberto@socialapps.com.co o spin.jag@gmail.com