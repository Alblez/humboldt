<div class="species index">
	<h2><?php echo __('Species'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('nombre_cientifico'); ?></th>
			<th><?php echo $this->Paginator->sort('kingdom'); ?></th>
			<th><?php echo $this->Paginator->sort('phylum'); ?></th>
			<th><?php echo $this->Paginator->sort('clase'); ?></th>
			<th><?php echo $this->Paginator->sort('orden'); ?></th>
			<th><?php echo $this->Paginator->sort('family'); ?></th>
			<th><?php echo $this->Paginator->sort('genus'); ?></th>
			<th><?php echo $this->Paginator->sort('specific_epithet'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($species as $species): ?>
	<tr>
		<td><?php echo h($species['Species']['nombre_cientifico']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['kingdom']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['phylum']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['clase']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['orden']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['family']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['genus']); ?>&nbsp;</td>
		<td><?php echo h($species['Species']['specific_epithet']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $species['Species']['id'])); ?>		
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registros para un total de {:count}, empezando en el registro {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Cargar Especies'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Listar Occurrences'), array('controller' => 'occurrences', 'action' => 'index')); ?> </li>
		<?php if ( $this->Paginator->params()['count'] > 0): ?>
			<li><?php echo $this->Html->link(__('Cargar Occurrences'), array('controller' => 'occurrences', 'action' => 'add')); ?> </li>
		<?php endif; ?>
	</ul>
</div>
