<div class="species view">
<h2><?php echo __('Species'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($species['Species']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('TaxonId'); ?></dt>
		<dd>
			<?php echo h($species['Species']['taxonId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre Cientifico'); ?></dt>
		<dd>
			<?php echo h($species['Species']['nombre_cientifico']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kingdom'); ?></dt>
		<dd>
			<?php echo h($species['Species']['kingdom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phylum'); ?></dt>
		<dd>
			<?php echo h($species['Species']['phylum']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Clase'); ?></dt>
		<dd>
			<?php echo h($species['Species']['clase']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Orden'); ?></dt>
		<dd>
			<?php echo h($species['Species']['orden']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Family'); ?></dt>
		<dd>
			<?php echo h($species['Species']['family']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Genus'); ?></dt>
		<dd>
			<?php echo h($species['Species']['genus']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Specific Epithet'); ?></dt>
		<dd>
			<?php echo h($species['Species']['specific_epithet']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rango'); ?></dt>
		<dd>
			<?php echo h($species['Species']['rango']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Autor'); ?></dt>
		<dd>
			<?php echo h($species['Species']['autor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre Comun'); ?></dt>
		<dd>
			<?php echo h($species['Species']['nombre_comun']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($species['Species']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($species['Species']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Species'), array('action' => 'edit', $species['Species']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Species'), array('action' => 'delete', $species['Species']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $species['Species']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Species'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Species'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Occurrences'), array('controller' => 'occurrences', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Occurrence'), array('controller' => 'occurrences', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Occurrences'); ?></h3>
	<?php if (!empty($species['Occurrence'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Species Id'); ?></th>
		<th><?php echo __('DatasetKey'); ?></th>
		<th><?php echo __('Publishing Org Key'); ?></th>
		<th><?php echo __('Publishing Country Id'); ?></th>
		<th><?php echo __('Protocol'); ?></th>
		<th><?php echo __('Last Crawled'); ?></th>
		<th><?php echo __('Last Parsed'); ?></th>
		<th><?php echo __('Extensions'); ?></th>
		<th><?php echo __('Basis Of Record'); ?></th>
		<th><?php echo __('Taxon Key'); ?></th>
		<th><?php echo __('Kingdom Key'); ?></th>
		<th><?php echo __('Phylum Key'); ?></th>
		<th><?php echo __('Class Key'); ?></th>
		<th><?php echo __('Order Key'); ?></th>
		<th><?php echo __('Family Key'); ?></th>
		<th><?php echo __('Genus Key'); ?></th>
		<th><?php echo __('Species Key'); ?></th>
		<th><?php echo __('Scientific Name'); ?></th>
		<th><?php echo __('Kingdom'); ?></th>
		<th><?php echo __('Phylum'); ?></th>
		<th><?php echo __('Order'); ?></th>
		<th><?php echo __('Family'); ?></th>
		<th><?php echo __('Genus'); ?></th>
		<th><?php echo __('Species'); ?></th>
		<th><?php echo __('Generic Name'); ?></th>
		<th><?php echo __('Specific Epithet'); ?></th>
		<th><?php echo __('Taxon Rank'); ?></th>
		<th><?php echo __('Continent'); ?></th>
		<th><?php echo __('State Province'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Event Date'); ?></th>
		<th><?php echo __('Issues'); ?></th>
		<th><?php echo __('LastInterpreted'); ?></th>
		<th><?php echo __('Identifiers'); ?></th>
		<th><?php echo __('Facts'); ?></th>
		<th><?php echo __('Relations'); ?></th>
		<th><?php echo __('Class'); ?></th>
		<th><?php echo __('Country Code'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Rights Holder'); ?></th>
		<th><?php echo __('Identifier'); ?></th>
		<th><?php echo __('InstitutionID'); ?></th>
		<th><?php echo __('Locality'); ?></th>
		<th><?php echo __('County'); ?></th>
		<th><?php echo __('Dataset Name'); ?></th>
		<th><?php echo __('GbifID'); ?></th>
		<th><?php echo __('Collection Code'); ?></th>
		<th><?php echo __('Language'); ?></th>
		<th><?php echo __('OccurrenceID'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Catalog Number'); ?></th>
		<th><?php echo __('RecordedBy'); ?></th>
		<th><?php echo __('Institution Code'); ?></th>
		<th><?php echo __('Rights'); ?></th>
		<th><?php echo __('IdentifiedBy'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($species['Occurrence'] as $occurrence): ?>
		<tr>
			<td><?php echo $occurrence['id']; ?></td>
			<td><?php echo $occurrence['species_id']; ?></td>
			<td><?php echo $occurrence['datasetKey']; ?></td>
			<td><?php echo $occurrence['publishing_org_key']; ?></td>
			<td><?php echo $occurrence['publishing_country_id']; ?></td>
			<td><?php echo $occurrence['protocol']; ?></td>
			<td><?php echo $occurrence['last_crawled']; ?></td>
			<td><?php echo $occurrence['last_parsed']; ?></td>
			<td><?php echo $occurrence['extensions']; ?></td>
			<td><?php echo $occurrence['basis_of_record']; ?></td>
			<td><?php echo $occurrence['taxon_key']; ?></td>
			<td><?php echo $occurrence['kingdom_key']; ?></td>
			<td><?php echo $occurrence['phylum_key']; ?></td>
			<td><?php echo $occurrence['class_key']; ?></td>
			<td><?php echo $occurrence['order_key']; ?></td>
			<td><?php echo $occurrence['family_key']; ?></td>
			<td><?php echo $occurrence['genus_key']; ?></td>
			<td><?php echo $occurrence['species_key']; ?></td>
			<td><?php echo $occurrence['scientific_name']; ?></td>
			<td><?php echo $occurrence['kingdom']; ?></td>
			<td><?php echo $occurrence['phylum']; ?></td>
			<td><?php echo $occurrence['order']; ?></td>
			<td><?php echo $occurrence['family']; ?></td>
			<td><?php echo $occurrence['genus']; ?></td>
			<td><?php echo $occurrence['species']; ?></td>
			<td><?php echo $occurrence['generic_name']; ?></td>
			<td><?php echo $occurrence['specific_epithet']; ?></td>
			<td><?php echo $occurrence['taxon_rank']; ?></td>
			<td><?php echo $occurrence['continent']; ?></td>
			<td><?php echo $occurrence['state_province']; ?></td>
			<td><?php echo $occurrence['date']; ?></td>
			<td><?php echo $occurrence['event_date']; ?></td>
			<td><?php echo $occurrence['issues']; ?></td>
			<td><?php echo $occurrence['lastInterpreted']; ?></td>
			<td><?php echo $occurrence['identifiers']; ?></td>
			<td><?php echo $occurrence['facts']; ?></td>
			<td><?php echo $occurrence['relations']; ?></td>
			<td><?php echo $occurrence['class']; ?></td>
			<td><?php echo $occurrence['country_code']; ?></td>
			<td><?php echo $occurrence['country_id']; ?></td>
			<td><?php echo $occurrence['rights_holder']; ?></td>
			<td><?php echo $occurrence['identifier']; ?></td>
			<td><?php echo $occurrence['institutionID']; ?></td>
			<td><?php echo $occurrence['locality']; ?></td>
			<td><?php echo $occurrence['county']; ?></td>
			<td><?php echo $occurrence['dataset_name']; ?></td>
			<td><?php echo $occurrence['gbifID']; ?></td>
			<td><?php echo $occurrence['collection_code']; ?></td>
			<td><?php echo $occurrence['language']; ?></td>
			<td><?php echo $occurrence['occurrenceID']; ?></td>
			<td><?php echo $occurrence['type']; ?></td>
			<td><?php echo $occurrence['catalog_number']; ?></td>
			<td><?php echo $occurrence['recordedBy']; ?></td>
			<td><?php echo $occurrence['institution_code']; ?></td>
			<td><?php echo $occurrence['rights']; ?></td>
			<td><?php echo $occurrence['identifiedBy']; ?></td>
			<td><?php echo $occurrence['created']; ?></td>
			<td><?php echo $occurrence['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'occurrences', 'action' => 'view', $occurrence['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'occurrences', 'action' => 'edit', $occurrence['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'occurrences', 'action' => 'delete', $occurrence['id']), array('confirm' => __('Are you sure you want to delete # %s?', $occurrence['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Occurrence'), array('controller' => 'occurrences', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
