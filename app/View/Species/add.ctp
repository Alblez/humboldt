<div class="species form">
<?php echo $this->Form->create('Species'); ?>
	<fieldset>
		<legend><?php echo __('Cargar Species'); ?></legend>
		<?php if (!empty($archivo)): ?>
			<h3><?php echo __('Archivo encontrado en: ') . $archivo; ?></h3>
		<?php else: ?>
			<h3><?php echo __('Archivo no encontrado en ruta predeterminada: '); ?></h3>
		<?php endif; ?>
	</fieldset>
<?php echo $this->Form->end(__('Cargar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listar Species'), array('action' => 'index')); ?></li>
	</ul>
</div>
