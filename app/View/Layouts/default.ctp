<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$titulo = __('Ejercicio presentado al Instituto Humboldt');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $titulo ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		
	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo $titulo; ?></h1>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
	<?php
		echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js');
		echo $this->fetch('script');
		echo $this->Js->writeBuffer();

	?>
	<script type="text/javascript">
	var url_api = "<?php echo $this->Html->url('/occurrences/getOcurrences', true); ?>";
	</script>
</body>
</html>
