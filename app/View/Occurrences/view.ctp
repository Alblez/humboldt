<div class="occurrences view">
<h2><?php echo __('Occurrence'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Species'); ?></dt>
		<dd>
			<?php echo $this->Html->link($occurrence['Species']['nombre_cientifico'], array('controller' => 'species', 'action' => 'view', $occurrence['Species']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DatasetKey'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['datasetKey']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Publishing Org Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['publishing_org_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Publishing Country Id'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['publishing_country_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Protocol'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['protocol']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Crawled'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['last_crawled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Parsed'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['last_parsed']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Extensions'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['extensions']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Basis Of Record'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['basis_of_record']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Taxon Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['taxon_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kingdom Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['kingdom_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phylum Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['phylum_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Class Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['class_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['order_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Family Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['family_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Genus Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['genus_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Species Key'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['species_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Scientific Name'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['scientific_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kingdom'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['kingdom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phylum'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['phylum']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Family'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['family']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Genus'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['genus']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Species'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['species']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Generic Name'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['generic_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Specific Epithet'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['specific_epithet']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Taxon Rank'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['taxon_rank']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Continent'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['continent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State Province'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['state_province']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Event Date'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['event_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Issues'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['issues']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LastInterpreted'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['lastInterpreted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Identifiers'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['identifiers']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Facts'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['facts']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Relations'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['relations']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Class'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['class']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country Code'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['country_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($occurrence['Country']['countryName'], array('controller' => 'countries', 'action' => 'view', $occurrence['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rights Holder'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['rights_holder']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Identifier'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['identifier']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('InstitutionID'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['institutionID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Locality'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['locality']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('County'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['county']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dataset Name'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['dataset_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('GbifID'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['gbifID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Collection Code'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['collection_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Language'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['language']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('OccurrenceID'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['occurrenceID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Catalog Number'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['catalog_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('RecordedBy'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['recordedBy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Institution Code'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['institution_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rights'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['rights']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('IdentifiedBy'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['identifiedBy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($occurrence['Occurrence']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Occurrence'), array('action' => 'edit', $occurrence['Occurrence']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Occurrence'), array('action' => 'delete', $occurrence['Occurrence']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $occurrence['Occurrence']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Occurrences'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Occurrence'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Species'), array('controller' => 'species', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Species'), array('controller' => 'species', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
	</ul>
</div>
