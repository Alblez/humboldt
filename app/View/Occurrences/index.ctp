<div class="occurrences index">
	<h2><?php echo __('Occurrences'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('specie'); ?></th>
			<th><?php echo $this->Paginator->sort('datasetKey'); ?></th>
			<th><?php echo $this->Paginator->sort('publishing_org_key'); ?></th>
			<th><?php echo $this->Paginator->sort('publishing_country_id'); ?></th>
			<th><?php echo $this->Paginator->sort('protocol'); ?></th>
			<th><?php echo $this->Paginator->sort('last_crawled'); ?></th>
			<th><?php echo $this->Paginator->sort('last_parsed'); ?></th>
			<th><?php echo $this->Paginator->sort('extensions'); ?></th>
			<th><?php echo $this->Paginator->sort('basis_of_record'); ?></th>
			<th><?php echo $this->Paginator->sort('taxon_key'); ?></th>
			<th><?php echo $this->Paginator->sort('kingdom_key'); ?></th>
			<th><?php echo $this->Paginator->sort('phylum_key'); ?></th>
			<th><?php echo $this->Paginator->sort('class_key'); ?></th>
			<th><?php echo $this->Paginator->sort('order_key'); ?></th>
			<th><?php echo $this->Paginator->sort('family_key'); ?></th>
			<th><?php echo $this->Paginator->sort('genus_key'); ?></th>
			<th><?php echo $this->Paginator->sort('species_key'); ?></th>
			<th><?php echo $this->Paginator->sort('scientific_name'); ?></th>
			<th><?php echo $this->Paginator->sort('kingdom'); ?></th>
			<th><?php echo $this->Paginator->sort('phylum'); ?></th>
			<th><?php echo $this->Paginator->sort('order'); ?></th>
			<th><?php echo $this->Paginator->sort('family'); ?></th>
			<th><?php echo $this->Paginator->sort('genus'); ?></th>
			<th><?php echo $this->Paginator->sort('species'); ?></th>
			<th><?php echo $this->Paginator->sort('generic_name'); ?></th>
			<th><?php echo $this->Paginator->sort('specific_epithet'); ?></th>
			<th><?php echo $this->Paginator->sort('taxon_rank'); ?></th>
			<th><?php echo $this->Paginator->sort('continent'); ?></th>
			<th><?php echo $this->Paginator->sort('state_province'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('event_date'); ?></th>
			<th><?php echo $this->Paginator->sort('issues'); ?></th>
			<th><?php echo $this->Paginator->sort('lastInterpreted'); ?></th>
			<th><?php echo $this->Paginator->sort('identifiers'); ?></th>
			<th><?php echo $this->Paginator->sort('facts'); ?></th>
			<th><?php echo $this->Paginator->sort('relations'); ?></th>
			<th><?php echo $this->Paginator->sort('class'); ?></th>
			<th><?php echo $this->Paginator->sort('country_code'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th><?php echo $this->Paginator->sort('rights_holder'); ?></th>
			<th><?php echo $this->Paginator->sort('identifier'); ?></th>
			<th><?php echo $this->Paginator->sort('institutionID'); ?></th>
			<th><?php echo $this->Paginator->sort('locality'); ?></th>
			<th><?php echo $this->Paginator->sort('county'); ?></th>
			<th><?php echo $this->Paginator->sort('dataset_name'); ?></th>
			<th><?php echo $this->Paginator->sort('gbifID'); ?></th>
			<th><?php echo $this->Paginator->sort('collection_code'); ?></th>
			<th><?php echo $this->Paginator->sort('language'); ?></th>
			<th><?php echo $this->Paginator->sort('occurrenceID'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('catalog_number'); ?></th>
			<th><?php echo $this->Paginator->sort('recordedBy'); ?></th>
			<th><?php echo $this->Paginator->sort('institution_code'); ?></th>
			<th><?php echo $this->Paginator->sort('rights'); ?></th>
			<th><?php echo $this->Paginator->sort('identifiedBy'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($occurrences as $occurrence): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($occurrence['Species']['nombre_cientifico'], array('controller' => 'species', 'action' => 'view', $occurrence['Species']['id'])); ?>
		</td>
		<td><?php echo h($occurrence['Occurrence']['datasetKey']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['publishing_org_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['publishing_country_id']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['protocol']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['last_crawled']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['last_parsed']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['extensions']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['basis_of_record']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['taxon_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['kingdom_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['phylum_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['class_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['order_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['family_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['genus_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['species_key']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['scientific_name']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['kingdom']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['phylum']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['order']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['family']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['genus']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['species']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['generic_name']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['specific_epithet']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['taxon_rank']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['continent']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['state_province']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['date']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['event_date']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['issues']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['lastInterpreted']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['identifiers']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['facts']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['relations']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['class']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['country_code']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($occurrence['Country']['countryName'], array('controller' => 'countries', 'action' => 'view', $occurrence['Country']['id'])); ?>
		</td>
		<td><?php echo h($occurrence['Occurrence']['rights_holder']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['identifier']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['institutionID']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['locality']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['county']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['dataset_name']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['gbifID']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['collection_code']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['language']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['occurrenceID']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['type']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['catalog_number']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['recordedBy']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['institution_code']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['rights']); ?>&nbsp;</td>
		<td><?php echo h($occurrence['Occurrence']['identifiedBy']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $occurrence['Occurrence']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registros para un total de {:count}, empezando en el registro {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Cargar Occurrences'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Reporte de Ocurrencias'), array('action' => 'report')); ?></li>
		<li><?php echo $this->Html->link(__('Ver Especies'), array('controller' => 'species', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Cargar Especies'), array('controller' => 'species', 'action' => 'add')); ?> </li>
	</ul>
</div>
