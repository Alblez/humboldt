<div class="occurrences form">
<?php echo $this->Form->create('Occurrence'); ?>
	<fieldset>
		<legend><?php echo __('Cargar Occurrence'); ?></legend>
		<?php 	echo $this->Form->input('species', array('type' => 'hidden', 'value' => json_encode($species))); ?>
	</fieldset>
<?php echo $this->Form->end(); ?>
	
	<div class="submit">
		<input value="Encontrar Occurrence" type="submit" id="encontrarOccurrences">
	</div>
	<div id="occurrencesResult">
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Reporte de Ocurrencias'), array('action' => 'report')); ?></li>
		<li><?php echo $this->Html->link(__('Ver Occurrences'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Ver Especies'), array('controller' => 'species', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Cargar Especies'), array('controller' => 'species', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php $this->Html->script('scripts', array('inline' => false)); ?>