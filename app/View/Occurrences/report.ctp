<div class="results index">
	<h2><?php echo __('Ocurrencias de especies por Pais'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('countryName','Pais'); ?></th>
		<th><?php echo $this->Paginator->sort('nombre_cientifico','Especie'); ?></th>
		<th><?php echo $this->Paginator->sort('Occurrences','Ocurrencias'); ?></th>
			
	</tr>
	</thead>
	<tbody>
<?php foreach ($results as $result): ?>
	<tr>
		
		<td><?php echo h($result['Country']['countryName']); ?>&nbsp;</td>
		<td><?php echo h($result['Species']['nombre_cientifico']); ?>&nbsp;</td>
		<td><?php echo h($result[0]['Occurrences']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Cargar Occurrences'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Ver Especies'), array('controller' => 'species', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Cargar Especies'), array('controller' => 'species', 'action' => 'add')); ?> </li>
	</ul>
</div>
