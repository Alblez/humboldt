<?php
App::uses('AppController', 'Controller');
/**
 * Species Controller
 *
 * @property Species $Species
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SpeciesController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Js');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session','Csv.Csv');
	
/**
 * Define propiedades para la clase
 */
	public $staticFile = WWW_ROOT . 'files' . DS . 'taxon.csv';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->Species->recursive = 0;
		$this->set('species', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Species->exists($id)) {
			throw new NotFoundException(__('Especie no encontrada'));
		}
		$options = array('conditions' => array('Species.' . $this->Species->primaryKey => $id));
		$this->set('species', $this->Species->find('first', $options));
	}

/**
 * add method
 * Procesa el archivo taxon
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->importFile();
		}
		if (file_exists($this->staticFile)) {
			$this->set('archivo', $this->staticFile);
		}
	}
	
/**
 * importFile method
 *
 * @return void
 */
	public function importFile() {
		
		$this->data = $this->csvToArray();

		if ($this->Species->saveAll($this->data)) {
			$this->Flash->success(__('Las especies han sido creadas en la base de datos.'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Flash->error(__('Las especies no han podido ser creadas en la base de datos, verifique el archivo.'));
		}

	}
	
/**
 * csvToArray method
 *
 * @return array()
 */
	private function csvToArray() {
		// campos del modelo species como key del arreglo
		$fields = array('taxonId',
				'nombre_cientifico',
				'kingdom',
				'phylum',
				'clase',
				'orden',
				'family',
				'genus',
				'specific_epithet',
				'rango',
				'autor',
				'nombre_comun');


		if (file_exists($this->staticFile)) {
			$taxon = $this->Csv->import($this->staticFile,$fields);
			if(empty($taxon)) {
				$this->Flash->error(__('Archivo vacio'));
				return $this->redirect(array('action' => 'index'));
			}
			unset($taxon[0]); // borra cabecera del archivo
		} else {
			$this->Flash->error(__('Archivo no encontrado en la ruta: ') . $this->staticFile);
			return $this->redirect(array('action' => 'index'));
		}
		$data = $this->newTaxons($taxon);

		return $data;
		//return $taxon;
		// eliminar registros duplicados del archivo taxon
		// foreach ($taxon as $key => $value) {
		// 	$a[$key] = $taxon[$key]['Species']['taxonId'];
		// }

	}
	
/**
 * newTaxons method
 * buscar registros iguales en la tabla species y archivo taxon y entrega solo los nuevos
 * evita crear duplicados
 * @return void
 */
	private function newTaxons($taxon) {

		$exists = array();
		$options = array('fields' => array('Species.taxonId'));
		$species = $this->Species->find('list', $options);

		// retorna el array en caso de no existir registros en la tabla species
		if (empty($species)) {
			return $taxon;
		}

		foreach ($taxon as $key => $value) {
			if (in_array($taxon[$key]['Species']['taxonId'], $species)) {
				$exists[] = $taxon[$key]['Species']['taxonId'];
				unset($taxon[$key]);
			}
		}

		if (empty($taxon)){
			$this->Flash->error(__('Todos los registros de este archivo ya se encuentran almacenados'));
			return $this->redirect(array('action' => 'index'));
		}

		return $taxon;
	}
	

}
