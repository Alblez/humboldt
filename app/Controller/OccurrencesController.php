<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
/**
 * Occurrences Controller
 *
 * @property Occurrence $Occurrence
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property RequestHandlerComponent $RequestHandler
 */
class OccurrencesController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Js', 'Time');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'RequestHandler');

/**
 * URL
 *
 * @var string
 */
	public $url = 'http://api.gbif.org/v1/occurrence/search';

/**
 * 
 *
 * @var array
 */
	public $successOcurrences = array();

	public $countries = array();

	public $errorOcurrences = array();


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Occurrence->recursive = 0;
		$this->set('occurrences', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Occurrence->exists($id)) {
			throw new NotFoundException(__('Invalid occurrence'));
		}
		$options = array('conditions' => array('Occurrence.' . $this->Occurrence->primaryKey => $id));
		$this->set('occurrence', $this->Occurrence->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		ini_set('memory_limit', '2048M');
		set_time_limit(0);
		if ($this->request->is('post')) {
			$this->Occurrence->create();
			if ($this->Occurrence->save($this->request->data)) {
				$this->Flash->success(__('The occurrence has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The occurrence could not be saved. Please, try again.'));
			}
			$this->getOcurrences($species);
		}
		$species = $this->Occurrence->Species->find('list');
		
		$this->set(compact('species'));
	}

/**
 * getOcurrencesBySpecie method
 *
 * @return array
 */
	private function getOcurrencesBySpecie($scientificName) {
		
		$httpSocket = new HttpSocket();
		$limit = 300;
		$data = array('scientificName' => '', 'offset' => 0, 'limit' => $limit);
		$data['scientificName'] = $scientificName;
		$specieOcurrences = array();
		$response = new stdClass();
		$response->body = array();
		$response->body['endOfRecords'] = false;

		while ( $response->body['endOfRecords'] !=  true) {
			$response = $httpSocket->get($this->url, $data);
			if ($response->code != 200) {
				$this->Flash->error(__('Error de conexion a la api, mientras procesaba la especie: ') . $scientificName);
				return $this->redirect(array('action' => 'index'));
			}

			$response->body = json_decode($response->body, true);
			if ($response->body['count']  > 200000) {
				break;
			}
			$specieOcurrences +=  Set::combine($response->body['results'], '{n}.key', '{n}');
			$data['offset'] += $limit;
			//break;
		}

		return $specieOcurrences;
	}

/**
 * getOcurrences method
 *
 * @return json
 */
	//public function getOcurrences($species) {
	public function getOcurrences() {		
		$this->autoRender = false;
		$this->layout = 'ajax';
		$specieId = $this->request->data['specieId'];
		$specieName = $this->request->data['scientificName'];
		$this->countries = array_flip($this->Occurrence->Country->find('list'));
		$occurrences = $this->prepareOcurrences($specieId, $this->getOcurrencesBySpecie($specieName));


		return json_encode($this->saveOccurrences($occurrences));
	}
/**
 * saveOccurrences method
 *
 * @return string
 */
	private function saveOccurrences($occurrences) {

		if (!empty($occurrences) && is_array($occurrences)) {
			
			if ($this->Occurrence->saveAll($occurrences)) {
				return count($occurrences);
			} else {
				return "Error al guardar " . count($occurrences);
			}
			
		}
	}
/**
 * prepareOcurrences method
 *
 * @return object
 */
	private function prepareOcurrences($specieId, $occurrences) {
		$data = array();
		foreach ($occurrences as $key => $occurrence) {
				$data[] = array('Occurrence' => array(
						'species_id' => $specieId,
						'key' => isset($occurrence['key'])? $occurrence['key'] : null ,
						'country_id' => $this->countries[$occurrence['countryCode']],
						'datasetKey' => isset($occurrence['datasetKey'])? $occurrence['datasetKey'] : null ,
						'publishing_org_key' => isset($occurrence['publishingOrgKey'])? $occurrence['publishingOrgKey'] : null ,
						'publishing_country_id' => isset($occurrence['publishingCountry'])? $occurrence['publishingCountry'] : null ,
						'protocol' => isset($occurrence['protocol'])? $occurrence['protocol'] : null ,
						'last_crawled' => isset($occurrence['lastCrawled'])? $occurrence['lastCrawled'] : null ,
						'last_parsed' => isset($occurrence['lastParsed'])? $occurrence['lastParsed'] : null ,
						'extensions' => isset($occurrence['extensions'])? $occurrence['extensions'] : null ,
						'basis_of_record' => isset($occurrence['basisOfRecord'])? $occurrence['basisOfRecord'] : null ,
						'taxon_key' => isset($occurrence['taxonKey'])? $occurrence['taxonKey'] : null ,
						'kingdom_key' => isset($occurrence['kingdomKey'])? $occurrence['kingdomKey'] : null ,
						'phylum_key' => isset($occurrence['phylumKey'])? $occurrence['phylumKey'] : null ,
						'class_key' => isset($occurrence['classKey'])? $occurrence['classKey'] : null ,
						'order_key' => isset($occurrence['orderKey'])? $occurrence['orderKey'] : null ,
						'family_key' => isset($occurrence['familyKey'])? $occurrence['familyKey'] : null ,
						'genus_key' => isset($occurrence['genusKey'])? $occurrence['genusKey'] : null ,
						'species_key' => isset($occurrence['speciesKey'])? $occurrence['speciesKey'] : null ,
						'scientific_name' => isset($occurrence['scientificName'])? $occurrence['scientificName'] : null ,
						'kingdom' => isset($occurrence['kingdom'])? $occurrence['kingdom'] : null ,
						'phylum' => isset($occurrence['phylum'])? $occurrence['phylum'] : null ,
						'order' => isset($occurrence['order'])? $occurrence['order'] : null ,
						'family' => isset($occurrence['family'])? $occurrence['family'] : null ,
						'genus' => isset($occurrence['genus'])? $occurrence['genus'] : null ,
						'species' => isset($occurrence['species'])? $occurrence['species'] : null ,
						'generic_name' => isset($occurrence['genericName'])? $occurrence['genericName'] : null ,
						'specific_epithet' => isset($occurrence['specificEpithet'])? $occurrence['specificEpithet'] : null ,
						'taxon_rank' => isset($occurrence['taxonRank'])? $occurrence['taxonRank'] : null ,
						'continent' => isset($occurrence['continent'])? $occurrence['continent'] : null ,
						'state_province' => isset($occurrence['stateProvince'])? $occurrence['stateProvince'] : null ,
						'date' => (isset($occurrence['year'])? $occurrence['year'] : '1900') . '-' . (isset($occurrence['month'])? $occurrence['month'] : '1') . '-' . (isset($occurrence['day'])? $occurrence['day'] : '1') ,
						'event_date' => isset($occurrence['eventDate'])? $occurrence['eventDate'] : null ,
						//'issues' => isset($occurrence['issues'])? $occurrence['issues'] : null ,
						'lastInterpreted' => isset($occurrence['lastInterpreted'])? $occurrence['lastInterpreted'] : null ,
						'identifiers' => isset($occurrence['identifiers'])? $occurrence['identifiers'] : null ,
						'facts' => isset($occurrence['facts'])? $occurrence['facts'] : null ,
						'relations' => isset($occurrence['relations'])? $occurrence['relations'] : null ,
						'class' => isset($occurrence['class'])? $occurrence['class'] : null ,
						'country_code' => isset($occurrence['countryCode'])? $occurrence['countryCode'] : null ,
						'rights_holder' => isset($occurrence['rightsHolder'])? $occurrence['rightsHolder'] : null ,
						'identifier' => isset($occurrence['identifier'])? $occurrence['identifier'] : null ,
						'institutionID' => isset($occurrence['institutionID'])? $occurrence['institutionID'] : null ,
						'locality' => isset($occurrence['locality'])? $occurrence['locality'] : null ,
						'county' => isset($occurrence['county'])? $occurrence['county'] : null ,
						'dataset_name' => isset($occurrence['datasetName'])? $occurrence['datasetName'] : null ,
						'gbifID' => isset($occurrence['gbifID'])? $occurrence['gbifID'] : null ,
						'collection_code' => isset($occurrence['collectionCode'])? $occurrence['collectionCode'] : null ,
						'language' => isset($occurrence['language'])? $occurrence['language'] : null ,
						'occurrenceID' => isset($occurrence['occurrenceID'])? $occurrence['occurrenceID'] : null ,
						'type' => isset($occurrence['type'])? $occurrence['type'] : null ,
						'catalog_number' => isset($occurrence['catalogNumber'])? $occurrence['catalogNumber'] : null ,
						'recordedBy' => isset($occurrence['recordedBy'])? $occurrence['recordedBy'] : null ,
						'institution_code' => isset($occurrence['institutionCode'])? $occurrence['institutionCode'] : null ,
						'rights' => isset($occurrence['rights'])? $occurrence['rights'] : null ,
						'identifiedBy' => isset($occurrence['identifiedBy'])? $occurrence['identifiedBy'] : null ,

						)
					);
				// if ( isset($occurrence['countryCode']) ) {
				// 	if (array_key_exists($occurrence['countryCode'], $this->countries)) {
				// 		$data['Occurrence']['country_id'] = $this->countries[$occurrence['countryCode']];
				// 	} else {
				// 		$data['Occurrence']['country_id'] = 0;
				// 	}
				// }
				$data += $data;
				
		}
		return $data; 
	}

/**
 * report method
 *
 * @return void
 */
	public function report() {
		
		$options = array(
				'fields' => array('Country.countryName','Species.nombre_cientifico','count(Occurrence.key) Occurrences'),
				'order' => array('Country.countryName'),
				'group' => array('Country.id','Species.id'),
			);
		$results = $this->Occurrence->find('all',$options);
		$this->set('results', $results);
	}

}
