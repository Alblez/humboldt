<?php
App::uses('AppModel', 'Model');
/**
 * Occurrence Model
 *
 * @property Species $Species
 * @property Country $Country
 */
class Occurrence extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'generic_name';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Species' => array(
			'className' => 'Species',
			'foreignKey' => 'species_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
