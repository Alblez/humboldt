<?php
App::uses('AppModel', 'Model');
/**
 * Species Model
 *
 * @property Occurrence $Occurrence
 */
class Species extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre_cientifico';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Occurrence' => array(
			'className' => 'Occurrence',
			'foreignKey' => 'species_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
